# Xen role
## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible xen -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt;"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash
allow_world_readable_tmpfiles=True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### download xen role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/xen.git
  name: xen
EOF
echo "roles/xen" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## xen (ubuntu)
Install and setup Xen server.
```bash
cat << 'EOF' > xen.yml
---
- hosts:
    xen
  tasks:
    - name: Xen install
      include_role:
        name: xen
      vars:
        xen_action: install
        xen_rep: 'http://mirror.yandex.ru/ubuntu'
        xen_root_pass: 'test1234'

EOF

ansible-playbook ./xen.yml
```
